import './styles/App.css'
import Routers from './routers/routers'
import ThemProvider from './layout/themProvider'
import { AuthProvider } from './routers/AuthProvider'

function App() {


  return (
    <AuthProvider> 
      <ThemProvider>
        <Routers/>
      </ThemProvider>
    </AuthProvider>
  )
}

export default App
