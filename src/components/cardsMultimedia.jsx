/* eslint-disable react/prop-types */
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import CardActions from '@mui/material/CardActions';
import { Link } from 'react-router-dom';


export default function CardsMultimedia({data}) {
  return (
    <Card sx={{ width: 245, maxHeight: 271, display: "flex", flexDirection: "column" }}>

        {data?.image &&
        <CardMedia
          component="img"
          height="100"
          image={data?.image}
          alt="green iguana"
        />
}
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {data?.title}
          </Typography>
          <Typography variant="body2" color="text.secondary" sx={{maxHeight: "53px", overflow: "auto"}}>
            {data?.text}
          </Typography>
        </CardContent>

        <CardActions sx={{justifySelf: "flex-start"}}>
        <Link size="small" to={"/detalle/"+data.id}>Ver</Link>

      </CardActions>
    </Card>
  );
}

