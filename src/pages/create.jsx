import { Grid, TextField, Typography } from "@mui/material";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import { useEffect, useState } from "react";
import Divider from "@mui/material/Divider";
import Button from "@mui/material/Button";
import CloudUploadIcon from "@mui/icons-material/CloudUpload";
import { styled } from "@mui/material/styles";
import { useForm } from "react-hook-form";

const fakeTematica = [
  {
    id: 1,
    nombre: "ciencia",
    permisos: { imagen: true, video: true, texto: true },
  },
  {
    id: 2,
    nombre: "arte",
    permisos: { imagen: true, video: true, texto: false },
  },
  {
    id: 3,
    nombre: "historia",
    permisos: { imagen: true, video: false, texto: true },
  },
];

const Create = () => {
  const [tematicaId, setTematicaId] = useState("");
  const [tematica, setTematica] = useState("");

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors, isDirty },
    watch,
    setValue,
  } = useForm({
    defaultValues: {
      titulo: "",
      texto: "",
      video: "",
      imagen: "",
    },
  });

  const handleChange = (event) => {
    setTematicaId(event.target.value);
    setTematica(fakeTematica.find((x) => x.id == event.target.value));
  };

  const onSubmit = handleSubmit(async (data) => {
    //data.email = data.trim();
    console.log(data);

    /*     api.post("login", data).then(
          response => {
          console.log('Datos recibidos:', response);
          login(response)
          navigate("/home", { replace: true });
          
        })
        .catch(error => {
          console.error('Error al cargar datos:', error);
        }); */
  });

  function getImageUrlFromFile(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();

        reader.onload = () => {
            resolve(reader.result);
        };

        reader.onerror = () => {
            reject(new Error('Error al leer el archivo'));
        };

        reader.readAsDataURL(file);
    });
}


useEffect(()=>{
  reset()
},[tematicaId])


  return (
    <Grid container spacing={"16px"}>
      <Grid item xs={12}>
        <Typography variant="h3">Crea un nuevo contenido</Typography>
      </Grid>
      <Grid item xs={3}>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">Tematica</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={tematicaId}
            label="Tematica"
            onChange={handleChange}
          >
            {fakeTematica &&
              fakeTematica.map((data, index) => (
                <MenuItem key={index} value={data.id}>
                  {data.nombre}
                </MenuItem>
              ))}
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <Divider sx={{ margin: "20px 0" }} />
      </Grid>
      <form onSubmit={onSubmit}>
        <Grid container spacing={"16px"}>
          <Grid item xs={12}>
            <TextField
              required
              id="outlined-required"
              label="Titulo"
              {...register("titulo", {required: {value: true, message: "requerido"}})}
              error={errors.titulo}
              helperText={errors.titulo ? errors.titulo.message : ""}
            />
          </Grid>

          {tematica?.permisos?.texto && (
            <Grid item xs={12}>
              <TextField
                fullWidth
                id="standard-multiline-flexible"
                label="Texto"
                multiline
                rows={4}
                maxRows={5}
                variant="outlined"
                {...register("texto")}
              />
            </Grid>
          )}

          {tematica?.permisos?.imagen && (
            <Grid item xs={6}>
              <TextField
                fullWidth
                id="outlined-required"
                label="YouTube URL"
                {...register("video")}
              />
            </Grid>
          )}

          {tematica?.permisos?.video && (
            <Grid item xs={3}>
              <Button
                component="label"
                role={undefined}
                variant="contained"
                tabIndex={-1}
                sx={{marginBottom: "16px"}}
                startIcon={<CloudUploadIcon />}
              >
                Subir imagen
                <VisuallyHiddenInput
                  type="file"
                  accept="image/*"
                  onChange={(e) => {
                    const file = e.target.files[0];
                    if (file && file.type.startsWith('image/')) {
                    getImageUrlFromFile(file)
                        .then(imageUrl => {
                       
                            setValue('imagen', imageUrl);
                        })
                        .catch(error => {
                            console.error(error);
                        });
                    }else {
                        console.log("error no es una imagen")
                    }

                    /* console.log(e.target.files[0]); */
                    
                  }}
                />
              </Button>
              <img style={{aspectRatio: "16/9", maxWidth: "300px"}} src={watch("imagen")}/>
            </Grid>
          )}

          {tematica &&
          <Grid item xs={12}>
            <Button type="submit" variant="contained" disabled={!isDirty}>Enviar</Button>
        </Grid> }
        </Grid>
      </form>

     
  {/*     <pre>{JSON.stringify(watch(), null, 2)}</pre> */}
    </Grid>
  );
};

export default Create;

const VisuallyHiddenInput = styled("input")({
  clip: "rect(0 0 0 0)",
  clipPath: "inset(50%)",
  height: 1,
  overflow: "hidden",
  position: "absolute",
  bottom: 0,
  left: 0,
  whiteSpace: "nowrap",
  width: 1,
});
