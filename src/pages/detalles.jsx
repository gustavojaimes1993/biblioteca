import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import useApi from "../hooks/useApi";
import { Box, CardMedia, Grid, Typography } from "@mui/material";
import {fakeMulti} from "../pages/home"
import ReactPlayer from 'react-player'

const Detalles = () => {
  const {id} = useParams();
  const api = useApi();
  const [data, setData] = useState(null);

  console.log(id);

  const getMultimedia = () => {
    /*         api
          .get(`detalle/${id}`, true)
          .then((response) => {
            //console.log(response);
            setData(response);
          })
          .catch((error) => {
            console.error("Error al cargar datos:", error);
          }); */
  };

  useEffect(() => {
    getMultimedia();
    setData(fakeMulti.find((x)=> x.id == id))

  }, [id]);
  return (
    <>
  {  data &&
    <Grid container sx={{ marginBottom: "64px" }}>
      <Grid item xs={12}>
        <Typography variant="h4">Detalles</Typography>
      </Grid>
      <Grid
        item
        xs={12}
        sx={{
          display: "flex",
          flexDirection: "column",
          gap: "16px",
          marginTop: "16px",
        }}
      >
        <Typography variant="h5">{data.title}</Typography>
        <Box sx={{ display: "flex", width: 560, margin: "auto" }}>
        {data?.image && <CardMedia
            component="img"
            height="315"
            image={
              data.image
            }
            alt="green iguana"
          />}
        </Box>
        <Typography variant="body1">
          {data?.text}
        </Typography>

        <Box sx={{ display: "flex", width: 560, margin: "auto" }}>
{/*           <iframe
            width="560"
            height="315"
            src="https://www.youtube.com/embed/Bo5pi4f9gWU"
            title="YouTube video player"
            frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
            referrerpolicy="strict-origin-when-cross-origin"
            allowfullscreen
          ></iframe> */}
          <ReactPlayer url={data.video} />
        </Box>
      </Grid>
    </Grid>}
    </>
  );
};

export default Detalles;
