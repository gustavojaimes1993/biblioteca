import useApi from "../hooks/useApi";
import "./../styles/login.css";
import { useForm } from "react-hook-form";
import useAuth from "../hooks/useAuth";
import { Link, useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { Button, Card, Grid, TextField } from "@mui/material";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";

const Register = () => {
  const { auth, login } = useAuth();
  const navigate = useNavigate();

  const api = useApi();

  const {
    register,
    handleSubmit,
    formState: { errors, isDirty },
    watch
  } = useForm({
    defaultValues: {
      email: "",
      password: "",
      repetirpassword: "",
      typeUser: "lector"
    },

  });
  //renderCount++;

  useEffect(() => {
    if (auth) {
      navigate("/home", { replace: true });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onSubmit = handleSubmit(async (data) => {
    //data.email = data.trim();
    console.log(data);

    /*     api.post("login", data).then(
            response => {
            console.log('Datos recibidos:', response);
            login(response)
            navigate("/home", { replace: true });
            
          })
          .catch(error => {
            console.error('Error al cargar datos:', error);
          }); */
  });

  return (
    <div className="body">
      <Card className="box">
        {/*  <h3>{renderCount}</h3> */}
        <h1>Registrarse</h1>
        <form onSubmit={onSubmit}>
          <Grid container spacing={"16px"}>
            <Grid item xs={12}>
              <TextField
                fullWidth
                required
                id="outlined-required"
                label="Correo"
                {...register("email", {
                  required: {
                    value: true,
                    message: "Correo es requerido",
                  },
                  pattern: {
                    value: /^[a-z0-9._%+-]+@[a-z0-9•-]+\.[a-z]{2,4}$/,
                    message: "Correo no válido",
                  },
                })}
                error={errors.email}
                helperText={errors.email ? errors.email.message : ""}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                fullWidth
                required
                id="outlined-required"
                label="Contraseña"
                type="password"
                {...register("password", {
                  required: {
                    value: true,
                    message: "Contraseña es requerido",
                  },
                  maxLength: {
                    value: 250,
                    message: "Maximo 250 caracteres",
                  },
                  minLength: {
                    value: 3,
                    message: "Minimo 3 caracteres",
                  },
                })}
                error={errors.password}
                helperText={errors.password ? errors.password.message : ""}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                fullWidth
                required
                id="outlined-required"
                label="Contraseña"
                type="password"
                {...register("repetirpassword", {
                  required: {
                    value: true,
                    message: "Contraseña es requerido",
                  },
                  maxLength: {
                    value: 250,
                    message: "Maximo 250 caracteres",
                  },
                  minLength: {
                    value: 3,
                    message: "Minimo 3 caracteres",
                  },
                  validate: value=> value === watch("password") || "Los passwords no coinciden"
                })}
                error={errors.repetirpassword}
                helperText={
                  errors.repetirpassword ? errors.repetirpassword.message : ""
                }
              />
            </Grid>
            <Grid item xs={12}>
              <FormControl>
                <FormLabel id="demo-controlled-radio-buttons-group">
                  Tipo de usuario
                </FormLabel>
                <RadioGroup
                  aria-labelledby="demo-controlled-radio-buttons-group"
                  name="controlled-radio-buttons-group"
                  
                  defaultValue={"lector"}
                  /*         value={value}
        onChange={handleChange} */
                >
                  <FormControlLabel
                    value="lector"
                    control={<Radio />}
                    label="Lector"
                    {...register("typeUser")}
                  />
                  <FormControlLabel
                    value="creador"
                    control={<Radio />}
                    label="Creador"
                    {...register("typeUser")}
                  />
                </RadioGroup>
              </FormControl>
            </Grid>
            <Grid
              item
              xs={12}
              sx={{ display: "flex", justifyContent: "center" }}
            >
              <Button type="submit" disabled={!isDirty} variant="contained">
                Iniciar session
              </Button>
            </Grid>
            <Grid
              item
              xs={12}
              sx={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Link to={`/login`}>Enviar</Link>
              <Link to={`/home`}>Volver</Link>
            </Grid>
          </Grid>
        </form>
{/*               <pre>
              {JSON.stringify(watch(), null, 2)}
              </pre>
              */}
        {/* <button onClick={()=> setSend(prev=>!prev)}>send</button>     */}
      </Card>
    </div>
  );
};

export default Register;
