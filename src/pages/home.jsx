import { Grid, Pagination, TextField } from "@mui/material";
import { useState } from "react";
import useTableFilters from "../hooks/useTableFilters";
import CardsMultimedia from "../components/cardsMultimedia";


export const fakeMulti = [
    {
        id: 1,
        title: "AMLO",
        text: "Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        image: "https://i1.wp.com/hr-gazette.com/wp-content/uploads/2018/08/bigstock-Mentoring-Concept-Mentor-At-T-237373231.jpg?fit=1600%2C1067&ssl=1",
        video: "https://www.youtube.com/watch?v=lT2Dvfgx2us"
    },
    {
        id: 2,
        title: "La playa",
        text: "Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        image: "https://w.wallhaven.cc/full/72/wallhaven-72yzje.jpg",
        video: "https://youtu.be/Bo5pi4f9gWU"
    },
    {
        id: 3,
        title: "Los mentores",
        text: "Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",


    },
    {
        id: 4,
        title: "Los mentores",
      
        image: "https://i1.wp.com/hr-gazette.com/wp-content/uploads/2018/08/bigstock-Mentoring-Concept-Mentor-At-T-237373231.jpg?fit=1600%2C1067&ssl=1",
        video: "https://youtu.be/Bo5pi4f9gWU"
    },
    {
        id: 5,
        title: "El estudio de ",
        text: "Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        image: "https://i1.wp.com/hr-gazette.com/wp-content/uploads/2018/08/bigstock-Mentoring-Concept-Mentor-At-T-237373231.jpg?fit=1600%2C1067&ssl=1",
        video: "https://youtu.be/Bo5pi4f9gWU"
    },
    {
        id: 6,
        title: "El ultimo",
        text: "Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        image: "https://i1.wp.com/hr-gazette.com/wp-content/uploads/2018/08/bigstock-Mentoring-Concept-Mentor-At-T-237373231.jpg?fit=1600%2C1067&ssl=1",
        video: "https://youtu.be/Bo5pi4f9gWU"
    },

    
]



const Home = () => {

  const [page, setPage] = useState(1);
  const [list, setList] = useState()

  let irequestFilter = [];
/*   irequestFilter.push({key: "search", value: ""}) */
  const [params, isLoading, setParams, handleSearch, handleSearchEmpty, UdateTable] = useTableFilters( "api/media", setList, false, irequestFilter );

  const handleChange = (event, value) => {
    setPage(value);
  };


  return (
    <Grid container>
      <Grid item xs={12} sm={12} md={6} lg={3} xl={2}>
        <TextField
          fullWidth
          id="outlined-required"
          label="Buscar"
          variant="standard"
          name="search"
          onChange={handleSearchEmpty}
          value={params.get("search") || ""}
        />
      </Grid>

      <Grid item xs={12} sx={{ display:"flex", flexWrap: "wrap", minHeight: "70vh", marginTop: "32px", gap: "16px" }}>
        {
            fakeMulti && fakeMulti.map((data, index)=>(

                <CardsMultimedia key={index} data={data}/>
            ))
        }
      </Grid>

      <Grid item xs={12} sx={{display: "flex", justifyContent: "center", margin: "26px 0px"}}>
        <Pagination count={10} page={Number(params.get("page")) } onChange={(e, value)=>handleSearch("page", value)} />
      </Grid>
    </Grid>
  );
};

export default Home;
