import {RouterProvider, createBrowserRouter} from "react-router-dom"
import Home from "../pages/home";
import Login from "../pages/login";
import Create from "../pages/create";
import Navbar from "../layout/navbar";
import Error from "../pages/erros";
import RequireAuth from "./RequireAuth";
import Register from "../pages/register";
import Detalles from "../pages/detalles";

const Routers = () => {

    const router = createBrowserRouter([
    {
      path: "/register",
      element: <Register />,
    },
    {
      path: "/login",
      element: <Login />,
    },

    {
      path: "/",
      element: <Navbar />,
      errorElement: <Error />,
      children: [
        {
          path: "/home",
          element: <Home />,
        },
        {
          path: "/detalle/:id",
          element: <Detalles />,
        },
        {
            path: "",
            element: <RequireAuth />,
            errorElement: <Error />,
            children: [
                {
                    path: "create",
                    element: <Create />,
                  },
            ]
        },

      ],
    },
    ])

    return ( 
        <RouterProvider router={router}>

        </RouterProvider>
     );
}
 
export default Routers;